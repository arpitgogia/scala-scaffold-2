ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.gitlab"
ThisBuild / organizationName := "secure"

lazy val root = (project in file("."))
  .settings(
    name := "test-scala",
    libraryDependencies ++= Seq(
      "com.hazelcast" % "hazelcast" % "3.12.12" % "compile",
      "commons-beanutils" % "commons-beanutils" % "1.9.4" % "compile",
      "com.amazonaws" % "aws-java-sdk-simpledb" % "1.12.187" % "compile",
      "io.vertx" % "vertx-core" % "4.2.6" % "compile",
      "io.vertx" % "vertx-web" % "4.2.6" % "compile",
      "org.owasp.encoder" % "encoder" % "1.2.3" % "compile",
      "org.opensaml" % "opensaml" % "2.6.4" % "compile",
      "org.opensaml" % "openws" % "1.5.4" % "compile",
      "org.opensaml" % "xmltooling" % "1.4.4" % "compile",
      "javax.servlet" % "javax.servlet-api" % "3.0.1" % "compile",
      "javax.faces" % "javax.faces-api" % "2.3" % "compile",
      "javax.el" % "javax.el-api" % "3.0.0" % "compile",
      "org.springframework.security" % "spring-security-core" % "5.6.2" % "compile",
      "org.apache.struts" % "struts2-core" % "2.3.30" % "compile",
      "org.springframework" % "spring-webmvc" % "5.3.17" % "compile",
      "org.apache.struts" % "struts-core" % "1.3.10" % "compile",
      "org.apache.xmlrpc" % "xmlrpc-client" % "3.1.3" % "compile",
      "org.apache.xmlrpc" % "xmlrpc-server" % "3.1.3" % "compile",
      "javax.activation" % "activation" % "1.1.1" % "compile",
      "javax.mail" % "javax.mail-api" % "1.6.2" % "compile",
      "org.apache.httpcomponents" % "httpclient" % "4.5.13" % "compile",
      "org.apache.velocity" % "velocity-engine-core" % "2.3" % "compile",
      "com.mitchellbosecke" % "pebble" % "2.4.0" % "compile",
      "commons-fileupload" % "commons-fileupload" % "1.4" % "compile",
      "org.springframework.security" % "spring-security-config" % "5.6.2" % "compile",
      "org.datanucleus" % "javax.jdo" % "3.2.0-release" % "compile",
      "com.google.guava" % "guava" % "31.1-jre" % "compile",
      "org.apache.commons" % "commons-text" % "1.9" % "compile",
      "org.apache.commons" % "commons-email" % "1.5" % "compile",
      "commons-io" % "commons-io" % "2.11.0" % "compile",
      "org.apache.wicket" % "wicket-core" % "9.9.0" % "compile",
      "javax.xml" % "jaxp-api" % "1.4.2" % "compile",
      "com.fasterxml.jackson.core" % "jackson-core" % "2.13.2" % "compile",
      "javax.ws.rs" % "javax.ws.rs-api" % "2.1.1" % "compile",
      "javax.jws" % "javax.jws-api" % "1.1" % "compile",
      "com.amazonaws" % "aws-java-sdk-simpledb" % "1.12.187" % "compile",
      "org.springframework.ldap" % "spring-ldap-core" % "2.3.6.RELEASE" % "compile",
      "com.opensymphony" % "xwork" % "2.1.0" % "compile",
      "org.hibernate" % "hibernate-core" % "5.3.2.Final" % "compile",
      "org.springframework" % "spring-jdbc" % "5.3.23" % "compile"
    )
  )
